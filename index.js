const express= require('express')
const logger = require('./logger')

const app = express()

app.use(express.json())


app.use('/api', require('./routes/apis'))


const PORT = process.env.PORT || 3000
app.listen(PORT,()=>logger.info(`Server started listening on http://localhost:${PORT}`));