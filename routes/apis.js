const dotenv = require('dotenv')
const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const uuid = require('uuid')
const authenticationMiddleware = require('../routes/middleware')
const logger = require('../logger')

dotenv.config()

const usersDatabase = []

let refreshTokens = []

//Sign Up
router.post('/signup', async (request,response) => {
    try {
        const isValidUsername = (username) => {
            const regex = /^[a-zA-Z ]{2,30}$/;
            return regex.test(username)
        }
        const isValidEmail = (email) => {
            const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
            return regex.test(email)
        }
        const isValidPassword = (password) => {
            return  ((password.length >= 5) && (password.length <= 16)) 
        }

        if (!isValidUsername(request.body.username)) {
            response.status(400).send(`USERNAME: ${request.body.username} is not valid. Characters in the username must be lies between 2 to 30.`)
        } else if (!isValidEmail(request.body.email)) {
            response.status(400).send(`EMAIL: ${request.body.email} is not valid.`)
        } else if (!isValidPassword(request.body.password)) {
            response.status(400).send("Characters in the password must be lies between 6 to 16")
        } else {
            const newUser = {
                id : uuid.v4(),
                username : request.body.username,
                email : request.body.email,
                password : request.body.password
            }

            const isNewUser = usersDatabase.find((user)=>(user.username === newUser.username) || (user.email === newUser.email))
            if (isNewUser) {
                response.status(401).send(`User with ${username} is already exits`)
                logger.error(`User with ${isNewUser.username} is already exists`)
            } else {
                const hashedPassword = await bcrypt.hash(request.body.password, 10)
                newUser.password = hashedPassword
                usersDatabase.push(newUser)
                response.status(201).send(newUser)
                logger.info({"message" : "User is created successfully."})
            }
        }
    }catch (error) {
        logger.error(`${error.message}`)
    }
})

//Sign In
router.post('/signin', async (request,response) => {
    try {
        const {username,password} = request.body
        const isUserValid = usersDatabase.find((user)=>user.username === username)

        if (isUserValid) {
            const isPasswordMatched = await bcrypt.compare(password, isUserValid.password)
            if (isPasswordMatched) {
                const payload = {
                    username : username
                }
                const jwtToken = generateAccessToken(payload)
                const refreshToken = jwt.sign(payload,process.env.REFRESH_TOKEN)
                refreshTokens.push(refreshToken)
                response.status(200).send({jwtToken,refreshToken})
                logger.info(`Successfully Signed In.`)
            } else {
                response.status(401).send("Incorrect password")
                logger.error("Incorrect Password")
            }
        }else {
            response.status(401).send("Invalid User")
            logger.error("Invalid User")
        }
    } catch (error) {
        logger.error(`${error.message}`)
    }
})

function generateAccessToken(payload) {
    return jwt.sign(payload,process.env.SECRET_TOKEN, {expiresIn: "1m"})
}

 //Resetting Password
router.patch('/reset', authenticationMiddleware, async (request,response) => {
    try {
        const {username,password, newPassword} = request.body
        const isValidUser = usersDatabase.find((user)=>user.username === username)
        if (isValidUser) {
            const isPasswordMatched = await bcrypt.compare(password,isValidUser.password)
            if (isPasswordMatched) {
                const newHashedPassword = await bcrypt.hash(newPassword,10)
                usersDatabase.forEach((user)=>{
                    if (user.username===isValidUser.username) {
                        user.password = newHashedPassword
                    }
                })
                response.status(200).send({message: `${username} : Password is changed successfully`})
                logger.info("Password is changed successfully.")
            } else {
                response.status(401).json({message :`${username}! password you entered is incorrect`})
                logger.error("Password you entered is INCORRECT.")
            }
        } else {
            response.status(401).send('Invalid User')
            logger.error("Invalid user")
        }
    }catch (error) {
        logger.error(`${error.message}`)
    }
})

//To generate access token
router.post('/token', (request,response)=>{
    try {
        const refreshToken = request.body.token

        if (refreshToken === null) {
            logger.error("Unauthorized User")
            response.status(401).send("Unauthorized user")
        }
        if (!refreshTokens.includes(refreshToken)) {
            logger.error("Invalid Refresh Token.")
            response.status(403).send("Forbidden")
        }
        jwt.verify(refreshToken,process.env.REFRESH_TOKEN, (error,user)=>{
            if (error) {
                response.status(403).send("Forbidden")
            }
            console.log(user)
            const accessToken = generateAccessToken({username:user.username})
            logger.info("New Access Token Generated.")
            response.send({accessToken})
        })
    } catch (error) {
        logger.error(`${error.message}`)
    }
})

//To delete the refresh token
router.delete('/delete', (request,response) => {
    try {
        refreshTokens = refreshTokens.filter((token)=>token !== request.body.token)
        response.json({"message" : "Refresh token is deleted successfully."})
        logger.info("Refresh token is deleted successfully.")
    } catch (error) {
        logger.error(`${error.message}`)
    }
})
module.exports = router
