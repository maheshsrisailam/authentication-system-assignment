const jwt = require('jsonwebtoken')
const logger = require('../logger')

const authenticationMiddleware = (request,respone,next) => {
    try {
        let jwtToken = null
        const authorizationHeader = request.headers["authorization"]
        if (authorizationHeader !== undefined) {
            jwtToken = authorizationHeader.split(" ")[1]
        }

        if (jwtToken !== undefined) {
            jwt.verify(jwtToken,process.env.SECRET_TOKEN, async (error) => {
                if (error) {
                    respone.status(403).send("Invalid Access Token.")
                    logger.error("Invalid Access Token.")
                } else {
                    next()
                }
            })
        }
    } catch (error) {
        logger.error(`${error.message}`)
    }

}

module.exports = authenticationMiddleware