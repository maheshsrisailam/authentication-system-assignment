const {createLogger, transports, format,timestamp} = require('winston')
 
const customFormat = format.combine(format.timestamp(),format.printf((info)=>{
    return `${info.timestamp} - [${info.level.toUpperCase().padEnd(7)}] - ${info.message}`
}))

const logger = createLogger({
    format : customFormat,
    transports:[
        new transports.Console(),
        new transports.File({filename: 'apiInfoMessages.log',
        level:'info',
        //format:format.combine(format.timestamp(),format.json())
    }),
    new transports.File({
        filename:'apiErrorMessages.log',
        level:'error',
        //format:format.combine(format.timestamp(),format.json())
    })
    ]
})
module.exports = logger;